/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.tictactoeexam.interfaces;

import com.progmatic.tictactoeexam.Cell;
import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.exceptions.CellException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ZsoCser
 */
public class BoardImpl implements Board {

    private Cell boardCell;

    BoardImpl() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setBoardCell(Cell boardCell) {
        this.boardCell = boardCell;
    }

    public BoardImpl(Cell boardCell) {
        this.boardCell = boardCell;
    }

    @Override
    public PlayerType getCell(int rowIdx, int colIdx) throws CellException {
        rowIdx = boardCell.getRow();
        colIdx = boardCell.getCol();
        if (rowIdx < 0 && rowIdx > 2 || colIdx < 0 && colIdx > 2) {
            String message = "A kérdezett sor vagy oszlop nincs a táblán.";
            throw new CellException(rowIdx, colIdx, message);
        }
        return boardCell.getCellsPlayer();
    }

    @Override
    public void put(Cell cell) throws CellException {
        int rowIdx = cell.getRow();
        int colIdx = cell.getCol();
        if (rowIdx < 0 && rowIdx > 2 || colIdx < 0 && colIdx > 2) {
            String message = "A kérdezett sor vagy oszlop nincs a táblán.";
            throw new CellException(rowIdx, colIdx, message);
        }
        if (cell.getCellsPlayer() != PlayerType.EMPTY) {
            String message = "Az adott cellában már van O vagy X!";
            throw new CellException(rowIdx, colIdx, message);
        }
        PlayerType akt = cell.getCellsPlayer();
    }

    @Override
    public boolean hasWon(PlayerType p) {
        
        
        int rowIdx = cell.getRow();
        int colIdx = cell.getCol();
        boolean won = false;
        if ((row == 0 && col == 1)
                && (row == 1 && col == 1)
                && (row == 2 && col == 0)
                && (row == 2 && col == 2)) //        Ha ezek üresek, p nem nyerthet
        {
            return won;
        } else {
            won = true;
        }

        return won;
    }

    @Override
    public List<Cell> emptyCells() {
        List<Cell> empty = new ArrayList<>();
        empty.add(cell.getCellsPlayer() = PlayerType.EMPTY);
        return empty;
    }
}
